from .set import Set
from .variable import Variable
import json

class Feature(Set):
    variables = []

    def __init__(self, id=None, name=None, data=None):
        self.id = data['id']
        self.guild = data['guild']
        self.name = data['name']
        self.value = data['value']
        self.short_description = data['short_description']
        self.description = data['description']
        self.enable = data['enable']
        self.variables = self._set_variables(data['variables'], data['guild_variables'])
        self.connection_pool = data['connection_pool']

    async def update(self, new_value):
        await self._update('features', new_value)

    def _set_variables(self, variables, guild_variables):
        result = []

        for variable in variables:
            if variable['feature_id'] != self.id:
                continue

            result.append(Variable(data={
                'id': variable['id'],
                'feature': self,
                'name': variable['name'],
                'guild': self.guild,
                'value': guild_variables.get(str(variable['id'])),
                'avaible_values': variable['avaible_values'],
                'short_description': variable['short_description'],
                'description': variable['description'],
                'enable': variable['enable'],
                'connection_pool': self.connection_pool
            }))

        return result
