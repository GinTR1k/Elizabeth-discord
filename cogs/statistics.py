import discord, asyncio
from discord.ext import commands

import time, datetime

class Statistics:
    def __init__(self, bot):
        self.bot = bot

        # Установка цвета полоски в сообщениях Embed для всего модуля
        self.embed_color = discord.Colour(0x00f0ff)

        # Текущая версия модуля
        self.__version__ = '1.0.0'

    @commands.command(name='uptime', help='Показать сколько времени бот работает')
    async def show_uptime(self, ctx):
        """ Показать uptime бота """

        # Точное время запуска
        started_at = datetime.datetime.fromtimestamp(self.bot.start_at).strftime('%H:%M:%S %d.%m.%Y')

        # Текущее время
        current_time = int(time.time())

        # uptime
        uptime = current_time - self.bot.start_at

        # Вычисляем количество дней, часов, минут и секунд uptime
        day = uptime // (24 * 3600)
        uptime = uptime % (24 * 3600)
        hour = uptime // 3600
        uptime %= 3600
        minutes = uptime // 60
        uptime %= 60
        seconds = uptime

        embed = discord.Embed(title='Elizabeth - Статистика', colour=self.embed_color,
            description=f"Бот работает вот уже \n{day} дней \n{hour} часов \n{minutes} минут \n{seconds} секунд.\n" +\
                        f"Был запущен {started_at} по МСК."
        )
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Statistics(bot))
