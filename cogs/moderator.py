import discord
from discord.ext import commands

class Moderator:
    def __init__(self, bot):
        self.bot = bot

        # Установка цвета полоски в сообщениях Embed для всего модуля
        self.embed_color = discord.Colour(0xffff00)

        # Текущая версия модуля
        self.version = '1.0.0'

    @commands.command(name='удалить', aliases=['очистить', 'del', 'delete', 'clear'], help='Команда для удаления сообщений')
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    async def messages_delete(self, ctx, messages_count=None):
        async def show_help(message=None):
            embed = discord.Embed(title="Команда `delete`", colour=self.embed_color, description="Удаляет сообщения от определенных пользователей или же последние сообщения.")

            embed.set_author(name='Помощь в использовании команд')
            embed.set_thumbnail(url="https://vignette.wikia.nocookie.net/clubpenguin/images/9/9b/Moderator_Badge_new.png/revision/latest?cb=20140206150417")
            embed.set_footer(text="Moderator")

            embed.add_field(name="Пример №1:", value="`delete 10` - удалить 10 последних сообщений в текущем канале.")
            embed.add_field(name="Пример №2:", value="`delete 25 @spammer#1234` - удалить все сообщения от пользователя **@spammer#1234** в пределах 25 последних сообщений в текущем канале.")
            embed.add_field(name="Пример №3:", value="`delete 50 @spammer#1234 @flooder#5647` - удалить все сообщения от пользователей **@spammer#1234** и **@flooder#5647** в пределах 50 последних сообщений в текущем канале.")
            embed.add_field(name="Примечание №1:", value="Возможно удалить максимум 100 сообщений за одну команду.")
            embed.add_field(name="Примечание №2:", value="Возможно удалить сообщения, которые не старше 14 дней.")

            await ctx.send(message, embed=embed)
        if messages_count is not None:
            if messages_count.isdigit():
                messages_count = int(messages_count)
                if messages_count > 0 and messages_count <= 100:
                    from_user = ctx.message.mentions

                    def purge_predicate(message):
                        if len(from_user) > 0:
                            return message.author in from_user
                        else:
                            return True

                    await ctx.message.delete()
                    deleted = await ctx.channel.purge(limit=messages_count, check=purge_predicate)

                    embed = discord.Embed(title='Команда `delete`', colour=self.embed_color, description=f'Удалено **{len(deleted)}** сообщений.')

                    embed.set_thumbnail(url="https://vignette.wikia.nocookie.net/clubpenguin/images/9/9b/Moderator_Badge_new.png/revision/latest?cb=20140206150417")
                    embed.set_footer(text="Moderator")

                    await ctx.send(embed=embed, delete_after=5)

                else:
                    await show_help(message='**Ошибка**: см. примечание №1')
            else:
                await show_help(message='**Ошибка**: см. помощь в использовании')
        else:
            await show_help(message='Помощь по команде')

def setup(bot):
    bot.add_cog(Moderator(bot))
