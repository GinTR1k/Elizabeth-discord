import discord
from discord.ext import commands

import sys, traceback

class Events:
    def __init__(self, bot):
        self.bot = bot

        # Текущая версия модуля
        self.__version__ = '0.0.1'

        # Вызываем async функцию
        bot.loop.create_task(self.cog_startup())

    async def cog_startup(self):
        """ Функция вызывается при запуске текущего cog """

        # Ожидаем готовности бота
        await self.bot.wait_until_ready()

        # Устанавливаем боту игру
        await self.bot.change_presence(
            activity=discord.Game(
                name='https://bot.xs.rs/'
            )
        )

    async def send_error(self, ctx, text):
        embed = discord.Embed(title="Ошибка", colour=discord.Colour(0xff0000), description="Произошла ошибка при выполнении команды")
        embed.set_footer(text=f"{self.__class__.__name__}, v{self.__version__}")

        embed.add_field(name="Дополнительные сведения", value=text, inline=False)

        await ctx.send(embed=embed)

    # Ошибка
    async def on_command_error(self, ctx, error):
        # This prevents any commands with local handlers being handled here in on_command_error.
        if hasattr(ctx.command, 'on_error'):
            return

        ignored = (commands.UserInputError,)

        # Allows us to check for original exceptions raised and sent to CommandInvokeError.
        # If nothing is found. We keep the exception passed to on_command_error.
        error = getattr(error, 'original', error)

        # Anything in ignored will return and prevent anything happening.
        if isinstance(error, ignored):
            return

        if isinstance(error, commands.CommandNotFound):
            return await self.send_error(ctx, f'Команды `{ctx.command}` не существует.')

        if isinstance(error, commands.DisabledCommand):
            return await self.send_error(ctx, f'Команда `{ctx.command}` была отключена разработчиком бота.')

        elif isinstance(error, commands.NoPrivateMessage):
            try:
                return await self.send_error(ctx, f'Команда `{ctx.command}` не может быть использована в Личных Сообщениях.')
            except:
                pass

        # For this error example we check to see where it came from...
        elif isinstance(error, commands.BadArgument):
            if ctx.command.qualified_name == 'tag list':  # Check if the command being invoked is 'tag list'
                return await self.send_error(ctx, f'Не могу найти этого пользователя. Пожалуйста, повторите попытку позднее.')

        # All other Errors not returned come here... And we can just print the default TraceBack.
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
        await self.send_error(ctx, f'Произошла непредвиденная ошибка во время выполнения команды `{ctx.command}`. Обратитесь к разработчику.')


def setup(bot):
    bot.add_cog(Events(bot))
