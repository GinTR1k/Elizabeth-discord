discord = {
    'token': '',
    'description': 'Мой автор - @GinTR1k#3691',
    'cogs_directory': 'cogs'
}

twitter = {
    'tokens': {
        'consumer_key'   : '',
        'consumer_secret': '',
        'access_token'   : '',
        'access_secret'  : ''
    },
    'cacheFile': 'DiscordTwitterCache'
}

database = {
    'host': 'localhost',
    'user': 'MySQL_user',
    'password': 'MySQL_userpassword',
    'db': 'MySQL_database',
    'charset': 'utf8',
    'autocommit': True
}

# Association user's games with server's roles
# 'name of game': 'role on server'
games = {
    'PLAYERUNKNOWN\'S BATTLEGROUNDS': 'PUBG',
    'Tom Clancy\'s Rainbow Six Siege': 'Rainbow Six Siege',
    'Grand Theft Auto V': 'Grand Theft Auto V',
    'Terraria': 'Terraria',
}

enable_system_channel = True
log_channel = 'general' # If enable_system_channel set to False
